package com.aras.modir.rxjavaexamples;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class ObservablesSamples {

    public static List<String> getNamesFromDB(String query) {
        List<String> names = new ArrayList<>();

        names.add("Alireza");
        names.add("Mohammad");
        names.add("Alireza");
        names.add("Javad");
        names.add("MAryam");

        return names;
    }
}
